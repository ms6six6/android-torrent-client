package com.github.axet.torrentclient.app;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.support.v7.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.AssetsDexLoader;
import com.github.axet.androidlibrary.app.RarSAF;
import com.github.axet.androidlibrary.app.ZipSAF;
import com.github.axet.androidlibrary.services.StorageProvider;
import com.github.axet.androidlibrary.sound.MediaPlayerCompat;
import com.github.axet.androidlibrary.sound.ProximityPlayer;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.torrentclient.activities.PlayerActivity;
import com.github.axet.torrentclient.services.TorrentContentProvider;

import net.lingala.zip4j.core.ZipFile;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.innosystec.unrar.Archive;
import de.innosystec.unrar.exception.RarException;
import de.innosystec.unrar.rarfile.FileHeader;
import libtorrent.Libtorrent;

public class TorrentPlayer {
    public static String TAG = TorrentPlayer.class.getSimpleName();

    public static final String PLAYER_NEXT = TorrentPlayer.class.getCanonicalName() + ".PLAYER_NEXT";
    public static final String PLAYER_PROGRESS = TorrentPlayer.class.getCanonicalName() + ".PLAYER_PROGRESS";
    public static final String PLAYER_STOP = TorrentPlayer.class.getCanonicalName() + ".PLAYER_STOP";
    public static final String PLAYER_PAUSE = TorrentPlayer.class.getCanonicalName() + ".PLAYER_PAUSE";

    public static final String OLD_PREFIX = "old_";

    public static final String CONTENTTYPE_PDF = "application/pdf";

    Context context;
    Handler handler = new Handler(Looper.getMainLooper());
    Storage.Torrent torrent;
    ArrayList<PlayerFile> files = new ArrayList<>();
    public String torrentHash;
    public String torrentName;
    Storage storage;
    MediaPlayerCompat player;
    int playingIndex = -1;
    Uri playingUri;
    PlayerFile playingFile;
    Runnable next;
    int video = -1; // index
    ProximityPlayer proximity;
    Runnable receiver;
    Runnable progress = new Runnable() {
        @Override
        public void run() {
            notifyProgress();
            handler.removeCallbacks(progress);
            handler.postDelayed(progress, AlarmManager.SEC1);
        }
    };
    Runnable saveDelay = new Runnable() {
        @Override
        public void run() {
            save(context, TorrentPlayer.this);
            saveDelay();
        }
    };

    public Decoder RAR = new Decoder() {
        ArrayList<Archive> aa = new ArrayList<>();

        @Override
        public boolean supported(TorFile f) {
            Uri u = Storage.child(context, torrent.path, f.file.getPath());
            String s = u.getScheme();
            if (s.equals(ContentResolver.SCHEME_CONTENT)) {
                return u.getPath().endsWith(".rar");
            } else if (s.equals(ContentResolver.SCHEME_FILE)) {
                File local = Storage.getFile(u);
                if (!local.exists())
                    return false;
                if (!local.isFile())
                    return false;
                return local.getName().toLowerCase().endsWith(".rar");
            } else {
                throw new Storage.UnknownUri();
            }
        }

        @Override
        public ArrayList<ArchiveFile> list(TorFile f) {
            try {
                ArrayList<ArchiveFile> ff = new ArrayList<>();
                Uri u = Storage.child(context, torrent.path, f.file.getPath());
                String s = u.getScheme();
                final Archive archive;
                if (s.equals(ContentResolver.SCHEME_CONTENT)) {
                    RarSAF local = new RarSAF(context, torrent.path, u);
                    archive = new Archive(new RarSAF(local));
                } else if (s.equals(ContentResolver.SCHEME_FILE)) {
                    File local = Storage.getFile(u);
                    archive = new Archive(new de.innosystec.unrar.NativeStorage(local));
                } else {
                    throw new Storage.UnknownUri();
                }
                List<FileHeader> list = archive.getFileHeaders();
                for (FileHeader h : list) {
                    if (h.isDirectory())
                        continue;
                    final FileHeader header = h;
                    ArchiveFile a = new ArchiveFile() {
                        @Override
                        public String getPath() {
                            return RarSAF.getRarFileName(header);
                        }

                        @Override
                        public InputStream open() throws IOException {
                            return new ParcelFileDescriptor.AutoCloseInputStream(new StorageProvider.ParcelInputStream() {
                                @Override
                                public void copy(OutputStream os) throws IOException {
                                    try {
                                        archive.extractFile(header, os);
                                        os.close();
                                    } catch (RarException e) {
                                        throw new IOException(e);
                                    }
                                }

                                @Override
                                public long getStatSize() {
                                    return header.getFullUnpackSize();
                                }

                                @Override
                                public void close() throws IOException {
                                    super.close();
                                }
                            });
                        }

                        @Override
                        public void copy(OutputStream os) throws IOException {
                            try {
                                archive.extractFile(header, os);
                            } catch (RarException e) {
                                throw new IOException(e);
                            }
                        }

                        @Override
                        public long getLength() {
                            return header.getFullUnpackSize();
                        }
                    };
                    ff.add(a);
                }
                return ff;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void clear() {
            try {
                for (Archive a : aa) {
                    a.close();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            aa.clear();
        }
    };

    public Decoder ZIP = new Decoder() {
        ArrayList<ZipFile> aa = new ArrayList<>();

        @Override
        public boolean supported(TorFile f) {
            Uri u = Storage.child(context, torrent.path, f.file.getPath());
            String s = u.getScheme();
            if (s.equals(ContentResolver.SCHEME_CONTENT)) {
                return u.getPath().endsWith(".zip");
            } else if (s.equals(ContentResolver.SCHEME_FILE)) {
                File local = Storage.getFile(u);
                if (!local.exists())
                    return false;
                if (!local.isFile())
                    return false;
                return local.getName().toLowerCase().endsWith(".zip");
            } else {
                throw new Storage.UnknownUri();
            }
        }

        @Override
        public ArrayList<ArchiveFile> list(TorFile f) {
            try {
                ArrayList<ArchiveFile> ff = new ArrayList<>();
                Uri u = Storage.child(context, torrent.path, f.file.getPath());
                String s = u.getScheme();
                final ZipFile zip;
                if (s.equals(ContentResolver.SCHEME_CONTENT)) {
                    zip = new ZipFile(new ZipSAF(context, torrent.path, u));
                } else if (s.equals(ContentResolver.SCHEME_FILE)) {
                    File local = Storage.getFile(u);
                    zip = new ZipFile(new net.lingala.zip4j.core.NativeStorage(local));
                } else {
                    throw new Storage.UnknownUri();
                }
                aa.add(zip);
                List list = zip.getFileHeaders();
                for (Object o : list) {
                    final net.lingala.zip4j.model.FileHeader zipEntry = (net.lingala.zip4j.model.FileHeader) o;
                    if (zipEntry.isDirectory())
                        continue;
                    ArchiveFile a = new ArchiveFile() {
                        @Override
                        public String getPath() {
                            return zipEntry.getFileName();
                        }

                        @Override
                        public InputStream open() {
                            try {
                                return zip.getInputStream(zipEntry);
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }

                        public void copy(OutputStream os) {
                            try {
                                InputStream is = zip.getInputStream(zipEntry);
                                IOUtils.copy(is, os);
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }

                        @Override
                        public long getLength() {
                            return zipEntry.getUncompressedSize();
                        }
                    };
                    ff.add(a);
                }
                return ff;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void clear() {
            aa.clear();
        }
    };

    Decoder[] DECODERS = new Decoder[]{RAR, ZIP};

    public static String getType(PlayerFile f) {
        return Storage.getTypeByName(f.getName());
    }

    public static boolean isVideo(String type) {
        if (type != null)
            return type.startsWith("video");
        return false;
    }

    public static boolean isSupported(String type) {
        if (type != null && !type.isEmpty()) {
            String[] skip = new String[]{"image/", "text/", CONTENTTYPE_PDF}; // MediaPlayer will open jpg and wait forever
            for (String s : skip) {
                if (type.startsWith(s))
                    return false;
            }
            String[] support = new String[]{"audio/", "video/"};
            for (String s : support) {
                if (type.startsWith(s))
                    return true;
            }
        }
        return true; // make default true
    }

    public static void save(Context context, TorrentPlayer player) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        save(player, edit);
        edit.commit();
    }

    public static void save(TorrentPlayer player, SharedPreferences.Editor edit) {
        if (player != null) {
            Uri uri = player.getStateUri();
            if (uri != null) {
                edit.putString(TorrentApplication.PREFERENCE_PLAYER, uri.toString());
                return;
            }
        }
        edit.remove(TorrentApplication.PREFERENCE_PLAYER);
    }

    public static void remove(Context context, String hash) {
        SharedPreferences shared = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        edit.remove(TorrentPlayer.OLD_PREFIX + hash);
        edit.commit();
    }

    public static String formatHeader(Context context, long pos, long dur) {
        String header = TorrentApplication.formatDuration(context, pos);
        if (dur > 0)
            header += OpenFileDialog.ROOT + TorrentApplication.formatDuration(context, dur);
        return header;
    }

    public static class State {
        public Uri state;
        public Uri uri;
        public String hash;
        public long t;

        public State(String u) {
            this(Uri.parse(u));
        }

        public State(Uri u) {
            state = u;
            String p = u.getPath();
            String[] pp = p.split(OpenFileDialog.ROOT);
            hash = pp[1];
            String v = u.getQueryParameter("t");
            if (v != null && !v.isEmpty())
                t = Integer.parseInt(v);
            uri = clearQuery(u);
        }
    }

    public static int compareFiles(String f1, String f2) { // can point to files only, no folders
        List<String> s1 = new ArrayList<>(Arrays.asList(Storage.splitPath(f1)));
        List<String> s2 = new ArrayList<>(Arrays.asList(Storage.splitPath(f2)));

        int c = Integer.valueOf(s1.size()).compareTo(s2.size());
        if (c != 0) { // not same folder, compare only parent folders
            s1.remove(s1.size() - 1);
            s2.remove(s2.size() - 1);
        }

        int m = Math.min(s1.size(), s2.size());
        for (int i = 0; i < m; i++) {
            String p1 = s1.get(i);
            String p2 = s2.get(i);
            int c2 = p1.compareTo(p2);
            if (c2 != 0)
                return c2;
        }

        return c;
    }

    public static class SortPlayerFiles implements Comparator<PlayerFile> {
        @Override
        public int compare(TorrentPlayer.PlayerFile file, TorrentPlayer.PlayerFile file2) {
            return compareFiles(file.getPath(), file2.getPath());
        }
    }

    public static class SortArchiveFiles implements Comparator<ArchiveFile> {
        @Override
        public int compare(ArchiveFile file, ArchiveFile file2) {
            return compareFiles(file.getPath(), file2.getPath());
        }
    }

    public static Uri clearQuery(Uri uri) {
        if (Build.VERSION.SDK_INT >= 11) {
            Uri.Builder b = uri.buildUpon();
            b.clearQuery();
            return b.build();
        } else {
            Uri.Builder b = uri.buildUpon();
            b.query("");
            return b.build();
        }
    }

    public static Uri fixRename(long t, Uri uri) { // if torrent was renamed, fix top folder
        ArrayList<String> pp = new ArrayList<>(uri.getPathSegments());
        String n = Libtorrent.torrentName(t);
        Uri.Builder b = uri.buildUpon();
        pp.set(1, n);
        b.path(TextUtils.join(OpenFileDialog.ROOT, pp));
        return b.build();
    }

    public static class Receiver extends BroadcastReceiver {
        Context context;

        public Receiver(Context context) {
            this.context = context;
            IntentFilter ff = new IntentFilter();
            ff.addAction(PLAYER_NEXT);
            ff.addAction(PLAYER_PROGRESS);
            ff.addAction(PLAYER_STOP);
            context.registerReceiver(this, ff);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
        }

        public void close() {
            context.unregisterReceiver(this);
        }
    }

    public interface Decoder {
        boolean supported(TorFile f);

        ArrayList<ArchiveFile> list(TorFile f);

        void clear();
    }

    public interface ArchiveFile {
        String getPath();

        InputStream open() throws IOException;

        void copy(OutputStream os) throws IOException;

        long getLength();
    }

    public static class TorFile {
        public long index; // libtorrent index
        public libtorrent.File file;

        public TorFile(long i, libtorrent.File f) {
            this.index = i;
            this.file = f;
        }
    }

    public class PlayerFile {
        public int index; // file index in directory / archive
        public int count; // directory count
        public Uri uri;
        public TorFile tor;
        public ArchiveFile arch;

        public PlayerFile(TorFile t) {
            this.tor = t;
            uri = TorrentContentProvider.getProvider().getUriForFile(torrentHash, t.file.getPath());
        }

        public PlayerFile(TorFile t, ArchiveFile f) {
            this.tor = t;
            this.arch = f;
            File ff = new File(tor.file.getPath(), arch.getPath());
            uri = TorrentContentProvider.getProvider().getUriForFile(torrentHash, ff.getPath());
        }

        public PlayerFile index(int i, int count) {
            this.index = i;
            this.count = count;
            return this;
        }

        public Uri getFile() {
            if (arch != null) // archive file
                return Storage.child(context, torrent.path, arch.getPath());
            else  // local file
                return Storage.child(context, torrent.path, tor.file.getPath());
        }

        public String getPath() {
            if (arch != null)
                return new File(tor.file.getPath(), arch.getPath()).toString();
            return tor.file.getPath();
        }

        public String getName() {
            if (arch != null)
                return new File(arch.getPath()).getName();
            return new File(tor.file.getPath()).getName();
        }

        public long getLength() {
            if (arch != null)
                return arch.getLength();
            return tor.file.getLength();
        }

        public boolean isLoaded() {
            return tor.file.getBytesCompleted() == tor.file.getLength();
        }

        public int getPercent() {
            return (int) (tor.file.getBytesCompleted() * 100 / tor.file.getLength());
        }

        public String toString() {
            return getName();
        }
    }

    public static class ExoLoader extends AssetsDexLoader.ThreadLoader {
        public static final Object lock = new Object();

        public ExoLoader(Context context, boolean block) {
            super(context, block, "exoplayer-core", "exoplayer-dash", "exoplayer-hsls", "exoplayer-smoothstreaming", "exoplayer-ui");
        }

        @Override
        public boolean need() {
            synchronized (lock) {
                return Build.VERSION.SDK_INT >= 14 && MediaPlayerCompat.classLoader == MediaPlayerCompat.class.getClassLoader();
            }
        }

        @Override
        public void done(ClassLoader l) {
            synchronized (lock) {
                MediaPlayerCompat.classLoader = l;
            }
        }
    }

    public TorrentPlayer(Context context, Storage storage, long t) {
        this(context, storage, storage.find(t));
    }

    public TorrentPlayer(final Context context, Storage storage, Storage.Torrent t) {
        this.context = context;
        this.storage = storage;
        this.torrent = t;

        new ExoLoader(context, true);

        receiver = new BroadcastReceiver() {
            BroadcastReceiver that = this;

            Runnable close = new Runnable() {
                @Override
                public void run() {
                    context.unregisterReceiver(that);
                }
            };

            {
                IntentFilter ff = new IntentFilter();
                ff.addAction(PLAYER_PAUSE);
                context.registerReceiver(that, ff);
            }

            @Override
            public void onReceive(Context context, Intent intent) {
                String a = intent.getAction();
                if (a.equals(PLAYER_PAUSE)) {
                    pause();
                }
            }
        }.close;

        update();
    }

    public void setContext(Context context) {
        this.context = context;
        if (proximity != null)
            proximity.setContext(context);
    }

    public Decoder getDecoder(TorFile f) {
        for (Decoder d : DECODERS) {
            if (d.supported(f))
                return d;
        }
        return null;
    }

    public void update() {
        Uri old = playingUri;

        torrentName = torrent.name();
        torrentHash = torrent.hash;

        long l = Libtorrent.torrentFilesCount(torrent.t);

        files.clear();
        ArrayList<PlayerFile> ff = new ArrayList<>();
        for (long i = 0; i < l; i++) {
            TorFile f = new TorFile(i, Libtorrent.torrentFiles(torrent.t, i));
            if (f.file.getCheck())
                ff.add(new PlayerFile(f).index((int) i, (int) l));
        }
        Collections.sort(ff, new SortPlayerFiles());

        for (Decoder d : DECODERS)
            d.clear();

        for (PlayerFile f : ff) {
            files.add(f);
            if (f.tor.file.getBytesCompleted() == f.tor.file.getLength()) {
                Decoder d = getDecoder(f.tor);
                if (d != null) {
                    try {
                        ArrayList<ArchiveFile> list = d.list(f.tor);
                        Collections.sort(list, new SortArchiveFiles());
                        int q = 0;
                        for (ArchiveFile a : list)
                            files.add(new PlayerFile(f.tor, a).index(q++, list.size()));
                    } catch (RuntimeException e) {
                        Log.d(TAG, "Unable to unpack zip", e);
                    }
                }
            }
        }

        if (old == null)
            return;
        TorrentPlayer.PlayerFile f = find(old);
        playingIndex = -1;
        if (f == null)
            return;
        playingIndex = files.indexOf(f);
    }

    public int getSize() {
        return files.size();
    }

    public int getPlaying() {
        return playingIndex;
    }

    public PlayerFile get(int i) {
        return files.get(i);
    }

    public PlayerFile find(Uri uri) {
        uri = clearQuery(uri);
        uri = fixRename(torrent.t, uri);
        for (PlayerFile f : files) {
            if (f.uri.equals(uri))
                return f;
        }
        return null;
    }

    public boolean open(Uri uri) {
        TorrentPlayer.PlayerFile f = find(uri);
        if (f == null)
            return false;
        return open(f);
    }

    public boolean open(PlayerFile f) {
        Log.d(TAG, "open " + f);
        final int i = files.indexOf(f);
        if (player != null) {
            player.release();
            player = null;
            video = -1;
        }
        playingIndex = i;
        playingUri = f.uri;
        playingFile = f;
        if (f.tor.file.getBytesCompleted() == f.tor.file.getLength()) {
            if (isSupported(TorrentPlayer.getType(f)))
                prepare(f.uri);
        }
        if (player == null) {
            next(i + 1);
            return false;
        }
        return true;
    }

    public void play(final int i) {
        handler.removeCallbacks(next);
        PlayerFile f = get(i);
        if (!open(f))
            return;
        play();
    }

    public void play() {
        if (video != getPlaying()) { // already playing video? just call start()
            String type = getType(playingFile);
            if (isVideo(type)) {
                PlayerActivity.startActivity(context);
                return;
            } else {
                if (video >= 0) {
                    PlayerActivity.closeActivity(context);
                    video = -1;
                }
            }
        }
        resume();
    }

    public void close(MediaPlayerCompat.PlayerView view) {
        if (isPlayingSound())
            pause();
        video = -1; // do not close player, keep seek progress
    }

    public void hide(MediaPlayerCompat.PlayerView view) {
        video = -1; // do not close player, keep seek progress
    }

    void prepare(final Uri uri) {
        player = MediaPlayerCompat.create(context, uri);
        player.addListener(new MediaPlayerCompat.Listener() {
            @Override
            public void onReady() {
                handler.post(new Runnable() { // allow remove pending progress notifications in close()
                    @Override
                    public void run() {
                        notifyProgress(); // update artwork
                    }
                });
            }

            @Override
            public void onEnd() {
                next(playingIndex + 1);
            }

            @Override
            public void onError(Exception e) {
                Log.e(TAG, "player", e);
                next(playingIndex + 1);
            }
        });
        player.prepare();
    }

    public Bitmap getArtwork() {
        if (player == null)
            return null;
        return player.getArtwork();
    }

    public void play(MediaPlayerCompat.PlayerView view) {
        video = playingIndex;
        Long seek = null;
        if (player != null) {
            seek = player.getCurrentPosition();
            player.release();
        }
        prepare(playingUri);
        view.setPlayer(player);
        if (seek != null)
            player.seekTo(seek);
        resume();
    }

    public void resume() {
        if (proximity != null)
            proximity.close();
        proximity = new ProximityPlayer(context) {
            @Override
            public void prepare() {
                player.setAudioStreamType(streamType);
            }
        };
        proximity.create();
        player.setPlayWhenReady(true);
        progress.run();
        saveDelay();
    }

    public void next(final int next) {
        if (player != null) { // next on error or end
            player.release();
            player = null;
        }
        if (proximity != null) {
            proximity.close();
            proximity = null;
        }
        handler.removeCallbacks(this.saveDelay);
        handler.removeCallbacks(this.progress);
        handler.removeCallbacks(this.next);
        this.next = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "next " + next);
                TorrentPlayer.this.next = null;
                if (next >= files.size()) {
                    stop();
                    notifyStop();
                    return; // n = 0;
                }
                PlayerFile f = get(next);
                boolean b = open(f);
                notifyNext();
                if (b)
                    play();
            }
        };
        handler.postDelayed(this.next, AlarmManager.SEC1);
    }

    public boolean isPlaying() { // actual playing mode (next or actual sound)
        if (next != null)
            return true;
        return isPlayingSound();
    }

    public boolean isPlayingSound() { // actual playing mode (actual sound)
        if (player == null)
            return false;
        return player.getPlayWhenReady();
    }

    public boolean isStop() {
        return player == null;
    }

    public void pause() {
        if (proximity != null) {
            proximity.close();
            proximity = null;
        }
        if (getPlaying() != -1) {
            if (player == null) {
                stop(); // clear next
                notifyStop();
                return;
            }
            if (isPlayingSound()) {
                player.setPlayWhenReady(false);
                notifyProgress();
                handler.removeCallbacks(progress);
                handler.removeCallbacks(saveDelay);
            } else {
                play();
            }
        }
    }

    public void stop() {
        Log.d(TAG, "stop");
        if (proximity != null) {
            proximity.close();
            proximity = null;
        }
        if (player != null)
            player.setPlayWhenReady(false);
        handler.removeCallbacks(progress);
        handler.removeCallbacks(next);
        handler.removeCallbacks(saveDelay);
        next = null;
        playingIndex = -1;
        playingUri = null;
        playingFile = null;
    }

    public void close() {
        Uri state = getStateUri();
        stop();
        if (player != null) {
            player.release();
            player = null;
            SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor edit = shared.edit();
            if (state != null)
                edit.putString(OLD_PREFIX + torrentHash, state.toString());
            else
                edit.remove(OLD_PREFIX + torrentHash);
            edit.commit();
        }
        if (receiver != null) {
            receiver.run();
            receiver = null;
        }
        handler.removeCallbacksAndMessages(null);
    }

    public long getTorrent() {
        return torrent.t;
    }

    public long getDuration() {
        return player.getDuration();
    }

    Intent notify(String a) {
        Intent intent = new Intent(a);
        intent.putExtra("t", torrent.t);
        if (player != null) {
            intent.putExtra("pos", player.getCurrentPosition());
            intent.putExtra("dur", getDuration());
            if (playingFile != null) {
                String[] ss = Storage.splitPath(playingFile.getName());
                intent.putExtra("title", TorrentApplication.DOTSLASH + ss[ss.length - 1]);
            }
        }
        return intent;
    }

    public void notifyNext() {
        Intent intent = notify(PLAYER_NEXT);
        intent.putExtra("play", player != null);
        context.sendBroadcast(intent);
    }

    public Intent notifyProgressIntent() {
        Intent intent = notify(PLAYER_PROGRESS);
        if (player != null)
            intent.putExtra("play", isPlaying() || next != null);
        else
            intent.putExtra("play", false);
        return intent;
    }

    public void notifyProgress() {
        Intent intent = notifyProgressIntent();
        context.sendBroadcast(intent);
    }

    public void notifyProgress(Receiver receiver) {
        Intent intent = notifyProgressIntent();
        receiver.onReceive(context, intent);
    }

    public void notifyStop() {
        handler.removeCallbacks(progress);
        Intent intent = new Intent(PLAYER_STOP);
        context.sendBroadcast(intent);
    }

    public Uri getStateUri() {
        if (player == null)
            return null;
        if (playingUri == null)
            return null;
        Uri.Builder b = playingUri.buildUpon();
        b.appendQueryParameter("t", "" + player.getCurrentPosition());
        return b.build();
    }

    public void seek(long i) {
        player.seekTo((int) i);
    }

    public String formatHeader() {
        if (player == null)
            return "";
        return formatHeader(context, player.getCurrentPosition(), getDuration());
    }

    public void saveDelay() {
        handler.removeCallbacks(saveDelay);
        handler.postDelayed(saveDelay, AlarmManager.MIN1);
    }
}
